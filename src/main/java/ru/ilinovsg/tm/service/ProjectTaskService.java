package ru.ilinovsg.tm.service;

import ru.ilinovsg.tm.entity.Project;
import ru.ilinovsg.tm.entity.Task;
import ru.ilinovsg.tm.exception.ProjectNotFoundException;
import ru.ilinovsg.tm.exception.TaskNotFoundException;
import ru.ilinovsg.tm.repository.AbstractRepository;
import ru.ilinovsg.tm.repository.ProjectRepository;
import ru.ilinovsg.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class ProjectTaskService extends AbstractRepository {

    private ProjectTaskService() {
    }

    private static ProjectTaskService instance = null;

    public static ProjectTaskService getInstance(){
        synchronized (ProjectTaskService.class) {
            if (instance == null) {
                instance = new ProjectTaskService();
            }
        }
        return instance;
    }

    ProjectRepository projectRepository = ProjectRepository.getInstance();

    TaskRepository taskRepository = TaskRepository.getInstance();

    public List<Task> findAllByProjectId(final Long projectId) {
        if (projectId == null) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    public Optional<Task> removeTaskFromProject (final Long projectId, final Long taskId) {
        final Optional<Task> task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if (task.isPresent()) {
            task.get().setProjectId(null);
        }
            return task;
    }

    public Optional<Task> addTaskToProject (final Long projectId, final Long taskId) throws ProjectNotFoundException, TaskNotFoundException {
        final Optional<Project> project = projectRepository.findById(projectId);
        final Optional<Task> task = taskRepository.findById(taskId);
        if (project.isPresent() && task.isPresent()) {
            task.get().setProjectId(projectId);
        }
        return task;
    }

    public void removeTasksAndProject (final Long projectId) throws ProjectNotFoundException, TaskNotFoundException {
        final Optional<Project> project = projectRepository.findById(projectId);
        if (project.isPresent()) {
            taskRepository.removeByProjectId(projectId);
            projectRepository.removeById(projectId);
        }
    }

}
